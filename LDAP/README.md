
Repositorio SOX
Samuel Ortiz


<p>Para empezar esta practica lo primero hay que instalar los servicios y poner contraseña:<p>

<p><img src="1.png"><p>
<p><img src="2.png"><p>
<p><img src="3.png"><p>

<p>Herramienta que nos permite administrar el LDAP<p>
<p><img src="4.png"><p>

<p>Ponemos el dominio: ubuntusrv.smx2023.net<p>
<p><img src="5.png"><p>

<p>Ponemos nombre de organización: smx2023<p>
<p><img src="6.png"><p>


<p><p>Nos dice si queremos borrar la base de datos y decimos que no
<p><img src="7.png"><p>


<p><p>Una vez aquí nos descargamos el siguiente archivo con este comando:
<p><img src="9.png"><p>


<p>Y lo descomprimimos con este comando:<p>
<p><img src="10.png"><p>


<p>Borramos el contenido de este directorio<p>
<p><img src="11.png"><p>

<p>Copiamos el archivo descomprimido a /phpldapadmin<p>
<p><img src="12.png"><p>

<p>Entramos desde el buscador (/la ip/phpldapadmin) y le damos a "login"<p>
<p><img src="13.png"><p>

<p>Iniciamos sesion con el dn(cn=admin,dc=ubuntusrv,dc=smx2023,dc=net) y contraseña<p>
<p><img src="14.png"><p>


<p>Vamos al archivo config.php y configuramos la siguiente linea:<p>
<p><img src="16.png"><p>


<p>A partir de aqui creamos las estructuras de usuarios en:<p>
<p><img src="17.png"><p>

<p>Ya podemos ver creada la siguiente estructura:<p>
<p><img src="18.png"><p>

<p>Con este comando en el servidor veremos la estructura creada anteriormente:<p>
<p><img src="19.png"><p>

<p>En la maquina servidor podemos ver como la estructura esta creada<p>
<p><img src="20.png"><p>