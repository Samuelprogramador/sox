`LDAP 2
SAMUEL ORTIZ`


<p>1.PARA EMPEZAR: Instalamos ldap en el cliente<p>
<p><img src="1.png"><p>
<p>2.Ponemos: "ubuntusrv.smx2023.net"<p>
<p><img src="2.png"><p>
<p>3.Le ponemos que LDAP va a usar:<p>
<p><img src="3.png"><p>
<p>4.Le decimos la versión 3 y le damos a crear root data base admin SI<p>
<p><img src="4.png"><p>
<p>5.Ahora que no<p>
<p><img src="5.png"><p>
<p>6.Le decimos que LDAP usara root<p>
<p><img src="6.png"><p>
<p>7.Ahora, en el server instalamos estos paquetes:<p>
<p><img src="7.png"><p>
<p>8.Una vez instalado, creamos el certificado con el siguiente comando:<p>
<p><img src="8.png"><p>
<p>9.Entramos en la ruta /etc/ssl/ y creamos el archivo ca.info:<p>`
<p><img src="9.png"><p>
<p>10.Creamos el CA con este comando:<p>
<p><img src="10.png"><p>
<p>11.Ahora actualizamos el certificado con: "update-ca-certificates"<p>
<p><img src="11.png"><p>
<p>12.Generamos la clave privada con "certtool --generate-privkey \"<p>
<p><img src="12.png"><p>
<p>13.Creamos la ruta /etc/ssl el archivo "ldap01.info"<p>
<p><img src="13.png"><p>
<p>14.Ahora creamos el certificado en el servidor:<p>
<p><img src="14.png"><p>
<p>15.Ponemos los permisos:<p>
<p><img src="15.png"><p>
<p>16.Creamos el siguiente archivo:<p>
<p><img src="16.png"><p>
<p>17.Actualizamos cambios con este comando: "sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f cerinfo.ldif"<p>
<p><img src="17.png"><p>
<p>18.En el siguiente fichero añadimos en slap_services esto siguiente:<p>
<p><img src="18.png"><p>
<p>19.Reiniciamos<p>
<p><img src="19.png"><p>
<p>20.Entramos en el /etc/hosts y ponemos lo siguiente:<p>
<p><img src="20.png"><p>
<p>21.Testeamos con este comando:<p>
<p><img src="21.png"><p>
<p>22.Ahora instalamos esto en el cliente:<p>
<p><img src="22.png"><p>
<p>23.Ahora descargamos el certificado del servidor con este comando:<p>
<p><img src="23.png"><p>
<p>24.Y verificamos el verificado:<p>
<p><img src="24.png"><p>
<p>25.Metemos lo siguiente en el fichero "/etc/ldap/ldap.conf"<p>
<p><img src="25.png"><p>
<p>26.Los permisos:<p>
<p><img src="26.png"><p>
<p>27.Como no nos deja hacer restart tendremos que instalar esto:<p>
<p><img src="27.png"><p>
<p>28.Ahora veremos que si funciona al hacer status:<p>
<p><img src="28.png"><p>
<p>29.Ponemos en el fichero "/etc/pam.d/common-session"<p>
<p><img src="29.png"><p>
<p>30.Comprobamos que si existen:<p>
<p><img src="30.png"><p>
<p>31.Creamos los 4 goblins:<p>
<p><img src="31.png"><p>
<p>32.Podremos meternos con cualquiera como podemos ver:<p>
<p><img src="32.png"><p>
